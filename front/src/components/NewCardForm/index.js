import React from 'react';

import {createCard} from '../../services/api';
import {useBoard} from '../../providers/board';
import {toastMessage} from '../../services/toastr';

import { StyledForm, StyledButton } from './NewCardForm.styled';


function NewCardForm() {
    const {boardState, setBoardState} = useBoard();

    const [newCardFormIsOpen, setNewCardFormIsOpen] = React.useState(false);

    const [titleField, setTitleField] = React.useState('');
    const [contentField, setContentField] = React.useState('');

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        if(!titleField || !contentField || titleField === "" || contentField === "") {
          toastMessage("Todos os campos são obrigatórios!");
          return;
        }
        try {
          const newCard = await createCard(titleField, contentField);
          setBoardState({...boardState, cardList: [...boardState.cardList, newCard]});
        } catch (error) {
          console.log(error);
        }finally{
          setTitleField('');
          setContentField('');
          setNewCardFormIsOpen(false);
        }
        
    }

  return (<>
      <StyledButton onClick={() => setNewCardFormIsOpen(!newCardFormIsOpen)} isOpen={newCardFormIsOpen} data-testid="new_card_form_toggle_button" >+</StyledButton>
      <StyledForm isOpen={newCardFormIsOpen} data-testid="new_card_form_block">
        <form onSubmit={handleFormSubmit}>
            <div>
              <label>Titulo:</label>
              <input type='text' value={titleField} onChange={(e) => setTitleField(e.target.value)} />
            </div>
            <div>
              <label>Conteúdo: </label>
              <textarea value={contentField} onChange={(e) => setContentField(e.target.value)} />
            </div>
            <button type='submit'>Criar</button>
        </form>
      </StyledForm>
    </>);
}

export default NewCardForm;