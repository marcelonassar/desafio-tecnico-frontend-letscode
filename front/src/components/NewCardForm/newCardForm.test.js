import { render, screen } from "@testing-library/react";
import userEvent from '@testing-library/user-event';

import NewCardForm from '.';

test("when the form is rendered, it should have a visible button", () => {
    render(<NewCardForm />);

    const toggleButton = screen.getByTestId('new_card_form_toggle_button');

    expect(toggleButton).toBeVisible();
});

test("when the form is rendered, the form block should not be visible", () => {
    render(<NewCardForm />);

    const formBlock = screen.getByTestId('new_card_form_block');

    expect(formBlock).not.toBeVisible();
});

test("when the toggle button is clicked, the block should become visible", () => {
    render(<NewCardForm />);

    const formBlock = screen.getByTestId('new_card_form_block');
    const toggleButton = screen.getByTestId('new_card_form_toggle_button');

    expect(formBlock).not.toBeVisible();

    userEvent.click(toggleButton);

    expect(formBlock).toBeVisible();
});