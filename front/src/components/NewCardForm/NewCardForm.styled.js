import styled from 'styled-components';

export const StyledForm = styled.div`
    position: absolute;
    width: 400px;
    height: 200px;
    padding: 1em 2em;
    margin-top: -10px;
    margin-left: 75px;
    border: 5px solid var(--standard-dark);
    border-radius: 0 20px 20px 20px;
    background-color: lightgray;
    opacity: ${({isOpen}) => isOpen ? '1' : '0'};
    z-index: ${({isOpen}) => isOpen ? '1' : '-1'};
    form {
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: space-evenly;
        input, textarea {
            width: 98.5%;
            border: none;
            border-radius: 10px;
        }
        textarea {
            height: 70px;
            padding: .3em;
        }
        input {
            padding: .3em;
        }
        button {
            border: none;
            background-color: var(--standard-dark);
            padding: 1em;
            border-radius: 10px;
            font-weight: bold;
            color: var(--standard-yellow);
            cursor: pointer;
            &:hover {
                background-color: var(--standard-darker);
            }
        }
    }
`

export const StyledButton = styled.button`
    margin: .3em 0 0 .3em;
    height: 70px;
    width: 70px;
    border: none;
    border-radius: 50% 50%;
    background-color: var(--standard-dark);
    cursor: pointer;
    font-size: 3rem;
    color: var(--standard-yellow);
    &:hover {
        background-color: var(--standard-darker);
    }
    ${({isOpen}) => isOpen ? "transform: rotate(45deg);" : ""}
    transition: 100ms;
`