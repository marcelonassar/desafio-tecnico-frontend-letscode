import React from 'react';

import Card from '../Card';

import {StyledColumn, StyledCardContainer} from './Column.styled'


function Column({cardList, title}) {

    const [$cards, setCards] = React.useState([]);
    React.useEffect(() => {
        setCards(cardList.map((card) => {
            return <Card initialTitle={card.titulo} initialContent={card.conteudo} initialList={card.lista} id={card.id} key={`${card.id}`} />
        }))
    }, [cardList]);

  return (<>
    <StyledColumn>
        <h3>{title}</h3>
        <StyledCardContainer data-testid="column_card_container">
          {$cards}
        </StyledCardContainer>
    </StyledColumn>
  </>);
}

export default Column;