import styled from 'styled-components';

export const StyledColumn = styled.div`
    width: 25%;
    height: 80vh;
    background-color: lightgray;
    margin-left: 30px;
    opacity: 0.9;
    border-radius: 20px;
    h3 {
        margin: 0;
        padding: .5em 0 .5em 1em;
        background-color: var(--standard-dark);
        border-radius: 20px 20px 0 0;
        color: var(--standard-yellow);
    }
`

export const StyledCardContainer = styled.div`
    height: 92%;
    overflow-x: hidden;
`