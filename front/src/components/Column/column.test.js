import { render, screen } from "@testing-library/react"

import Column from '.';

test("when the column is rendered, it should have a title", () => {
    const mockedList = [{titulo: "Card1", conteudo: 'conteudo1', initialList: 'ToDo', id: '1'}, {titulo: "Card2", conteudo: 'conteudo2', initialList: 'ToDo', id: '2'}, {titulo: "Card3", conteudo: 'conteudo3', initialList: 'ToDo', id: '3'}]
    render(<Column title="Column Title" cardList={mockedList} />);

    const title = screen.getByRole('heading', {name: /column title/i});

    expect(title).toBeVisible();
})

test("when the column is rendered, it should have the right number of cards", () => {
    const mockedList = [{titulo: "Card1", conteudo: 'conteudo1', initialList: 'ToDo', id: '1'}, {titulo: "Card2", conteudo: 'conteudo2', initialList: 'ToDo', id: '2'}, {titulo: "Card3", conteudo: 'conteudo3', initialList: 'ToDo', id: '3'}]
    render(<Column title="Column Title" cardList={mockedList} />);

    const cardContainer = screen.getByTestId("column_card_container");

    expect(cardContainer.childNodes).toHaveLength(3);
})

