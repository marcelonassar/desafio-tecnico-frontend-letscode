import { render, screen } from "@testing-library/react"
import userEvent from '@testing-library/user-event'

import Card from "."

test("when the card is rendered, it should have a title", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    const title = screen.getByRole("heading", {name: /card title/i});

    expect(title).toBeVisible();
})

test("when the card is rendered, it should have a content", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    const content = screen.getByTestId('card_content')

    expect(content).toBeVisible();
    expect(content).toContainHTML('<p>card content</p>');
})

test("when the card is rendered, it should have a edit button", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    const editButton = screen.getByTestId('card_edit_button')

    expect(editButton).toBeVisible();
})

test("when the card is rendered, it should have a delete button", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    const deleteButton = screen.getByTestId('card_delete_button')

    expect(deleteButton).toBeVisible();
})

test("when the card list is ToDo, it should have a forward button", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    const forwardButton = screen.getByTestId('card_forward_button');

    expect(forwardButton).toBeEnabled();
});

test("when the card list is Doing, it should have a backward and a forward button", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="Doing" id="1"  />);

    const backwardButton = screen.getByTestId('card_backward_button');
    const forwardButton = screen.getByTestId('card_forward_button');

    expect(backwardButton).toBeEnabled();
    expect(forwardButton).toBeEnabled();
})

test("when the card list is Done, it should have a backward button", () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="Done" id="1"  />);

    const backwardButton = screen.getByTestId('card_backward_button')

    expect(backwardButton).toBeEnabled();
})

test("when the edit button is clicked, the edit form should be rendered", async () => {
    render(<Card initialTitle="card title" initialContent='card content' initialList="ToDo" id="1"  />);

    userEvent.click(screen.getByTestId('card_edit_button'))
    const editForm = await screen.findByTestId("card_edit_form")

    expect(editForm).toBeVisible();
    expect(screen.getByRole('button', {name: 'Cancelar'})).toBeEnabled();
    expect(screen.getByRole('button', {name: 'Salvar'})).toBeEnabled();
})