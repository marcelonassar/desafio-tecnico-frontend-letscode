import React from 'react';
import { FaTrashAlt, FaChevronRight, FaChevronLeft, FaEdit } from 'react-icons/fa';
import { marked } from 'marked';
import DOMPurify from 'dompurify';

import { useBoard } from '../../providers/board';
import { updateCard, deleteCard } from '../../services/api';
import { toastMessage } from '../../services/toastr';

import { StyledCard, StyledIconButton } from './Card.styled';

const lists = ['ToDo', 'Doing', 'Done'];

function Card({ initialTitle, initialContent, initialList, id }) {
  const {boardState, setBoardState} = useBoard()
  const [title, setTitle] = React.useState(initialTitle);
  const [content, setContent] = React.useState(initialContent);
  const [listIndex, setListIndex] = React.useState(lists.findIndex((listName) => listName === initialList));

  const [isEditing, setIsEditing] = React.useState(false);

  const calcelEditingHandler = () => {
    setTitle(initialTitle);
    setContent(initialContent);
    setIsEditing(false);
  }

  const submitEditingHandler = async () => {
    if(!title || !content || title === "" || content === "") {
      toastMessage("Todos os campos são obrigatórios!");
      return;
    }
    try {
      await updateCard(id, title, content, initialList);
      setIsEditing(false);
    } catch (error) {
      console.log(error);
      setTitle(initialTitle);
      setContent(initialContent);
      setIsEditing(false);
    }
  }

  const changeListHandler = async (newListIdx) => {
    try {
      const updatedCard = await updateCard(id, title, content, lists[newListIdx]);
      const updatedCardList = boardState.cardList.map((card) => {
        if(card.id === id){
          return updatedCard;
        }
        return card
      });
      setBoardState({...boardState, cardList: updatedCardList});
    } catch (error) {
      console.log(error);
    }
  }

  const deleteCardHandler = async () => {
    try {
      const updatedCardList = await deleteCard(id);
      setBoardState({...boardState, cardList: updatedCardList});
    } catch (error) {
      console.log(error);
    }
  }

  if(isEditing){
    return (<StyledCard>
      <form data-testid="card_edit_form">
        <button style={{marginBottom: '.5em'}} onClick={calcelEditingHandler}>Cancelar</button>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <label htmlFor='titleField'>Titulo:</label><input name='titleField' type='text' value={title} onChange={(e) => setTitle(e.target.value)} />
          <label htmlFor='contentField'>Conteúdo: </label><textarea name='contentField' value={content} onChange={(e) => setContent(e.target.value)} />
          <button type='button' onClick={submitEditingHandler}>Salvar</button>
        </div>
      </form>
    </StyledCard>)
  }

  return (<>
    <StyledCard> 
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
          <StyledIconButton onClick={() => setIsEditing(true)} data-testid="card_edit_button" ><FaEdit /></StyledIconButton>
          <h4 style={{overflow: 'auto', maxWidth: "65%"}}>{title}</h4>
          <StyledIconButton onClick={() => deleteCardHandler()} data-testid="card_delete_button" ><FaTrashAlt /></StyledIconButton>
        </div>
        <div dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(marked(content))}} data-testid="card_content" ></div>
        <div style={{display: 'flex', justifyContent: 'space-between'}}>
          {listIndex > 0 ? <StyledIconButton onClick={() => changeListHandler(listIndex - 1)} data-testid="card_backward_button" ><FaChevronLeft /></StyledIconButton> : <div></div>}
          {listIndex < 2 && <StyledIconButton onClick={() => changeListHandler(listIndex + 1)} data-testid="card_forward_button" ><FaChevronRight /></StyledIconButton>}
        </div>
    </StyledCard>
  </>);
}

export default Card;