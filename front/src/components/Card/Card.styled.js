import styled from 'styled-components';

export const StyledCard = styled.div`
    margin: 1em;
    padding: .5em;
    background-color: lightgray;
    opacity: 1;
    border-radius: 20px;
    border: 2px solid var(--standard-dark);
    input {
        width: 100%;
        padding: 3px 5px;
        margin: 8px 0;
        box-sizing: border-box;
    }
    textarea {
        width: 100%;
        margin: 8px 0;
        box-sizing: border-box;
        height: 70px;   
    }
    form {
        font-family: Roboto, Arial, sans-serif;
        input, textarea {
            border: none;
            border-radius: 10px;
        }
        textarea {
            padding: .3em;
        }
        button {
            border: none;
            background-color: var(--standard-dark);
            padding: 1em;
            border-radius: 10px;
            font-weight: bold;
            color: var(--standard-yellow);
            cursor: pointer;
            &:hover {
                background-color: var(--standard-darker);
            }
        }
    }
`
export const StyledIconButton = styled.button`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    border: none;
    color: var(--standard-yellow);
    background-color: var(--standard-dark);
    cursor: pointer;
    &:hover {
        background-color: var(--standard-darker);
    }
`