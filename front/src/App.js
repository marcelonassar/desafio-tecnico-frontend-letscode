import {login} from './services/login';
import { BoardProvider } from './providers/board';
import { ToastContainer } from 'react-toastify';

import Board from './pages/Board';

import 'react-toastify/dist/ReactToastify.css';
import './App.css'

let token = localStorage.getItem("userTOKEN");
if(!token){
  login().then((tk) => {
    localStorage.setItem("userToken", tk);
    token = tk
  })
}

function App() {
  return (
    <div className="App">
      <BoardProvider>
        <Board />
      </BoardProvider>
      <ToastContainer />
    </div>
  );
}

export default App;
