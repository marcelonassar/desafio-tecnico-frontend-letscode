import axios from 'axios';

import {login} from './login';
import {toastMessage} from './toastr';

//Normalmente colocaria em uma variável de ambiente, mas para não enviar um .env para o git, deixei mockado para esse projeto
const API_URL = 'http://localhost:5000';

let token = localStorage.getItem("userToken");

const reLogin = async () => {
    const tk = await login();
    localStorage.setItem("userToken", tk);
    token = tk;
}

export const fetchCards = async () => {
    try {
        const { data } = await axios.get(`${API_URL}/cards/`, {headers: {
            "Authorization": `Bearer ${token}`
        }});
        return data;
    } catch (error) {
        if(error.message === "Request failed with status code 401"){
            toastMessage("Erro ao validar usuário, atualize a página e tente novamente");
        } else {
            toastMessage("Erro ao buscar lista de cards, atualize a página e tente novamente");
        }
        await reLogin();
        throw new Error("erro ao fazer requisição para API")
    }
}

export const createCard = async (title, content) => {
    try {
        const cardAttributes = {
            titulo: title,
            conteudo: content,
            lista: 'ToDo'
        }
        const { data } = await axios.post(`${API_URL}/cards/`, cardAttributes, {headers: {
            "Authorization": `Bearer ${token}`
        }});
        return data;
    } catch (error) {
        if(error.message === "Request failed with status code 401"){
            toastMessage("Erro ao validar usuário, atualize a página e tente novamente");
        } else {
            toastMessage("Erro ao criar card, atualize a página e tente novamente");
        }
        await reLogin();
        throw new Error("erro ao fazer requisição para API")
    } 
}

export const updateCard = async (id, title, content, list) => {
    try {
        const cardAttributions = {
            id: id, 
            titulo: title, 
            conteudo: content,
            lista: list
        };
        const { data } = await axios.put(`${API_URL}/cards/${id}`, cardAttributions, {headers: {
            "Authorization": `Bearer ${token}`
        }});
        return data;
    } catch (error) {
        if(error.message === "Request failed with status code 401"){
            toastMessage("Erro ao validar usuário, atualize a página e tente novamente");
        } else {
            toastMessage("Erro ao atualizar card, atualize a página e tente novamente");
        }
        await reLogin();
        throw new Error("erro ao fazer requisição para API")
    }
}

export const deleteCard = async (id) => {
    try {
        const { data } = await axios.delete(`${API_URL}/cards/${id}`, {headers: {
            "Authorization": `Bearer ${token}`
        }});
        return data;
    } catch (error) {
        if(error.message === "Request failed with status code 401"){
            toastMessage("Erro ao validar usuário, atualize a página e tente novamente");
        } else {
            toastMessage("Erro ao deletar card, atualize a página e tente novamente");
        }
        await reLogin();
        throw new Error("erro ao fazer requisição para API")
    }
}