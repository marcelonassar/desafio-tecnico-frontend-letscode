import React from 'react';

import Column from '../../components/Column';
import NewCardForm from '../../components/NewCardForm';

import { fetchCards } from '../../services/api';
import { useBoard } from '../../providers/board';

import { StyledBoard } from './Board.styled';

function Board() {
    const {boardState, setBoardState} = useBoard();
  
    React.useEffect(async () => {
        try {
            const cards = await fetchCards();
            if(Array.isArray(cards)){
                setBoardState({...boardState, cardList: cards});
            }else{
                throw new Error("cardList só pode ser do tipo array")
            }
        } catch (error) {
            console.log(error)
        }
    }, []);

    

    return (
    <StyledBoard>
        <NewCardForm />
        <div style={{display:'flex', marginTop: '30px', justifyContent: 'space-around'}}>
            <Column cardList={boardState.cardList.filter((card) => card.lista === 'ToDo')} key='todo' title="To do" />
            <Column cardList={boardState.cardList.filter((card) => card.lista === 'Doing')} key='doing' title='Doing'/>
            <Column cardList={boardState.cardList.filter((card) => card.lista === 'Done')} key='done' title='Done' />
        </div>
        
    </StyledBoard>);
}

export default Board;