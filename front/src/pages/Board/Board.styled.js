import styled from 'styled-components';
import imgUrl from '../../assets/background.svg'

export const StyledBoard = styled.div`
    height: 100vh;
    background-color: var(--standard-dark);
    background-image: url(${imgUrl});
    background-repeat: no-repeat;
    background-size: cover;
    font-family: Montserrat, sans-serif;
`