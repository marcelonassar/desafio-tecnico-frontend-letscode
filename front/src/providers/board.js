import React, {useState} from 'react';

export const BoardContext = React.createContext({});

export const BoardProvider = (props) => {
    const [boardState, setBoardState] = useState({
        cardList: [],
    });

    return (
        <BoardContext.Provider value={{boardState, setBoardState}} >
            {props.children}
        </BoardContext.Provider>
    )
}

export const useBoard = () => React.useContext(BoardContext);