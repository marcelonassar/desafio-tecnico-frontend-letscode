# Desafio técnico Let's code Front End

Passo a passo de como configurar o projeto do desafio Let's code

## Rodando a API
Para deixar o servidor da api rodando, execute os comandos:

```console
cd BACK
npm install
npm run dev
```

Ele responderá na porta 5000.

## Rodando a aplicação react

Para subir o servidor react, execute os comandos: 

```console
cd front
npm install
npm start
```

Ele responderá na porta 3000

Agora acesse http://localhost:3000/ no navegador.